package name.li.pain.api;

import static com.lightbend.lagom.javadsl.api.Service.named;
import static com.lightbend.lagom.javadsl.api.Service.pathCall;

import java.util.Map;
import java.util.Optional;

import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;

import akka.Done;
import akka.NotUsed;
import akka.stream.javadsl.Source;

public interface PainService extends Service {

	ServiceCall<NotUsed, Done> rate(String userId, Integer value);

	ServiceCall<NotUsed, Optional<UserRating>> getRating(String userId);

	ServiceCall<NotUsed, Map<String, Integer>> getAllRatings();

	ServiceCall<NotUsed, Source<UserRatingsStreamEvent, NotUsed>> ratings(Optional<String> offset);

	@Override
	default Descriptor descriptor() {
		return named("painrater").withCalls(
				pathCall("/api/rate?userId&value", this::rate),
				pathCall("/api/allRatings", this::getAllRatings),
				pathCall("/api/getRate?userId", this::getRating),
				pathCall("/api/ratings?ratings", this::ratings))
				.withAutoAcl(true);
	}

}
