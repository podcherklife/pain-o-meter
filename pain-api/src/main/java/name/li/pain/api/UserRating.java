package name.li.pain.api;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserRating {

	private Instant timestamp;
	private Integer value;
	private String userId;

	public UserRating(
			@JsonProperty("value") Integer value,
			@JsonProperty("userId") String userId,
			@JsonProperty("timestamp") Instant timestamp) {
		super();
		this.value = value;
		this.userId = userId;
		this.timestamp = timestamp;
	}

	public Integer getValue() {
		return value;
	}

	public String getUserId() {
		return userId;
	}

	public Instant getTimestamp() {
		return timestamp;
	}

}
