package name.li.pain.api;

import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@Immutable
@JsonDeserialize(builder = ImmutableUserRatingsStreamEvent.Builder.class)
@JsonPOJOBuilder(withPrefix = "")
public interface UserRatingsStreamEvent {
	
	@JsonProperty("rating")
	UserRating rating();

	@JsonProperty("offset")
	String offset();

}
