package name.li.pain.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.Instant;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.BiFunction;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.Sets;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.testkit.ServiceTest;

import akka.Done;
import akka.NotUsed;
import akka.stream.javadsl.Source;
import akka.stream.testkit.TestSubscriber.Probe;
import akka.stream.testkit.javadsl.TestSink;
import name.li.pain.api.PainService;
import name.li.pain.api.UserRating;
import name.li.pain.api.UserRatingsStreamEvent;
import name.li.pain.users.api.ImmutableTag;
import name.li.pain.users.api.UserService;
import name.li.pain.users.api.UserState;
import name.li.pain.utils.test.LagomServiceTest;
import scala.concurrent.duration.FiniteDuration;

public class PainServiceTest extends LagomServiceTest {

	@BeforeClass
	public static void start() {
		setup(defaultSetup().withConfigureBuilder(
				b -> b.overrides(ServiceTest.bind(UserService.class).toInstance(fakeUserService()))));
	}

	@AfterClass
	public static void stop() {
		tearDown();
	}

	@Test
	public void shouldSaveAndReturnRating() throws Exception {
		PainService service = service(PainService.class);
		String userId = buildIdForTest("User");
		int rating = 5;

		get(service.rate(userId, rating).invoke());
		int savedRating = get(service.getRating(userId).invoke()).get().getValue();

		assertEquals(rating, savedRating);
	}

	@Test
	public void shouldListRatingInAllRatings() throws Exception {
		PainService service = service(PainService.class);

		String userId = buildIdForTest("User");
		Integer rating = 5;

		get(service.rate(userId, rating).invoke());
		Thread.sleep(TimeUnit.MILLISECONDS.convert(10, TimeUnit.SECONDS));
		Map<String, Integer> allRatings = get(service.getAllRatings().invoke());

		Integer userRating = allRatings.get(userId);

		assertEquals(rating, userRating);
	}
	
	@Test
	public void shouldStreamAllNewRatings() throws Exception {
		PainService service = service(PainService.class);
		
		String user1 = buildIdForTest("User1");
		Integer rating1 = 1;
		String user2 = buildIdForTest("User2");
		Integer rating2 = 5;

		service.rate(user1, rating1).invoke();
		service.rate(user2, rating2).invoke();
		
		Probe<UserRatingsStreamEvent> probe = ratingsStreamProbe(service);
		
		probe.request(2);
		UserRating r1 = probe.expectNext(ratingsProbeTimeout()).rating();
		UserRating r2 = probe.expectNext(ratingsProbeTimeout()).rating();
		probe.cancel();
		
		BiFunction<Integer, String, String> makeRatingKey = (rating, name) -> name + "|" + rating;
		
		HashSet<String> expectedRatings = Sets.newHashSet(
				makeRatingKey.apply(rating1, user1),
				makeRatingKey.apply(rating2, user2));
		HashSet<String> actualRatings = Sets.newHashSet(
				makeRatingKey.apply(r1.getValue(), r1.getUserId()),
				makeRatingKey.apply(r2.getValue(), r2.getUserId()));
		
		assertEquals(expectedRatings, actualRatings);
	}
	
	@Test
	public void shouldAssignSaneTimestampsToRatings() throws Exception {
		PainService service = service(PainService.class);

		String user1 = buildIdForTest("User1");
		Integer rating1 = 1;
		Instant expectedTimeRatingCreated = Instant.now();
		service.rate(user1, rating1).invoke();
		
		Probe<UserRatingsStreamEvent> probe = ratingsStreamProbe(service);

		probe.request(1);
		UserRatingsStreamEvent event = probe.expectNext(ratingsProbeTimeout());
		Instant timeWhenGotRating = Instant.now();
		
		Instant ratingTimestamp = event.rating().getTimestamp();
		assertTrue(ratingTimestamp.isBefore(timeWhenGotRating));
		assertTrue(ratingTimestamp.isAfter(expectedTimeRatingCreated));

	}

	private FiniteDuration ratingsProbeTimeout() {
		return FiniteDuration.create(10, TimeUnit.SECONDS);
	}

	private Probe<UserRatingsStreamEvent> ratingsStreamProbe(PainService service)
			throws InterruptedException, ExecutionException, TimeoutException {
		Source<UserRatingsStreamEvent, ?> ratings = get(service.ratings(Optional.empty()).invoke());
		Probe<UserRatingsStreamEvent> probe = ratings.runWith(TestSink.probe(server().system()), server().materializer());
		return probe;
	}

	public static UserService fakeUserService() {
		return new UserService() {

			@Override
			public ServiceCall<Set<ImmutableTag>, Done> updateTags(String userId) {
				return null;
			}

			@Override
			public ServiceCall<NotUsed, String> newUser(String name) {
				return null;
			}

			@Override
			public ServiceCall<NotUsed, UserState> getUser(String userId) {
				return req -> CompletableFuture.completedFuture(new UserState(userId, userId, Collections.emptySet()));
			}

			@Override
			public ServiceCall<NotUsed, Set<ImmutableTag>> getTags(String userId) {
				return null;
			}

			@Override
			public ServiceCall<NotUsed, List<UserState>> getAllUsers() {
				return null;
			}
		};
	}

}
