package name.li.pain.impl.commands;

import com.lightbend.lagom.javadsl.persistence.PersistentEntity;

import akka.Done;

public class ChangePainRate extends PainMessage implements PersistentEntity.ReplyType<Done> {
	private Integer rate;

	public ChangePainRate(Integer rate) {
		super();
		this.rate = rate;
	}
	
	public Integer getRate() {
		return rate;
	}
}
