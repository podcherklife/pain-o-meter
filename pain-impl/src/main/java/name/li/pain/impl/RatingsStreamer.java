package name.li.pain.impl;

import java.util.Optional;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;

import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.persistence.Offset;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;

import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.japi.Pair;
import akka.japi.pf.PFBuilder;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Source;
import name.li.pain.api.ImmutableUserRatingsStreamEvent;
import name.li.pain.api.UserRating;
import name.li.pain.api.UserRatingsStreamEvent;
import name.li.pain.impl.events.PainEvent;
import name.li.pain.impl.events.PainRateChanged;
import name.li.pain.utils.OffsetDaoFactory;
import name.li.pain.utils.OffsetDaoFactory.OffsetDao;
import name.li.pain.utils.OffsetStringifier;
import scala.PartialFunction;

public class RatingsStreamer {

	private PersistentEntityRegistry registry;
	private ActorSystem actorSystem;
	private OffsetDao offsetDao;

	@Inject
	public RatingsStreamer(
			PersistentEntityRegistry registry,
			OffsetDaoFactory offsetDaoFactory,
			ActorSystem actorSystem) {
		super();
		this.registry = registry;
		this.actorSystem = actorSystem;
		this.offsetDao = offsetDaoFactory.prepare("ratingsStream");
		init();
	}

	private void init() {
		initOffsetUpdating();
	}

	private Source<Pair<PainEvent, Offset>, NotUsed> getRawEventsStream(Optional<Offset> offset) {
		return registry.eventStream(PainRateChanged.TAG, offset.orElse(Offset.NONE));
	}

	private void initOffsetUpdating() {
		getStoredOffset()
				.thenAccept(offset -> {
					getRawEventsStream(offset)
							.map(pair -> pair.second())
							.runForeach(this::updateDefaultOffset, ActorMaterializer.create(actorSystem));
				});
	}

	public ServiceCall<NotUsed, Source<UserRatingsStreamEvent, NotUsed>> ratings(Optional<String> offset) {
		PartialFunction<Pair<PainEvent, Offset>, UserRatingsStreamEvent> collector = new PFBuilder<Pair<PainEvent, Offset>, UserRatingsStreamEvent>()
				.match(Pair.class, p -> (p.first() instanceof PainRateChanged), (pair -> {
					PainRateChanged evt = (PainRateChanged) pair.first();
					return ImmutableUserRatingsStreamEvent.builder()
							.rating(new UserRating(evt.getNewRate(), evt.getEntityId(), evt.getTimestamp()))
							.offset(OffsetStringifier.offsetToString((Offset) pair.second()))
							.build();
				}))
				.build();

		return req -> getStoredOffset()
				.thenApply(storedOffset -> getRawEventsStream(
						offset.map(OffsetStringifier::stringToOffset).map(Optional::of).orElse(storedOffset))
								.collect(collector));
	}

	public ServiceCall<NotUsed, Source<UserRatingsStreamEvent, NotUsed>> ratings() {
		return ratings(Optional.empty());
	}

	public CompletionStage<Optional<Offset>> getStoredOffset() {
		return offsetDao.lastSavedOffset();
	}

	public void updateDefaultOffset(Offset offset) {
		offsetDao.saveOffset(offset);
	}

}
