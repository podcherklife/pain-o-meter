package name.li.pain.impl;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletionStage;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraReadSideProcessor;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraSession;

import name.li.pain.impl.events.PainEvent;
import name.li.pain.impl.events.PainRateChanged;

public class PainRateEventProcessor extends CassandraReadSideProcessor<PainEvent> {

	private PreparedStatement updatePain;
	private PreparedStatement updateOffset;

	@Override
	public AggregateEventTag<PainEvent> aggregateTag() {
		return PainEvent.TAG;
	}

	@Override
	public EventHandlers defineEventHandlers(EventHandlersBuilder eb) {
		eb.setEventHandler(PainRateChanged.class, (evt, offset) -> {

			BoundStatement up = updatePain.bind()
				.setString("userId", evt.getEntityId())
				.setInt("rating", evt.getNewRate());

			BoundStatement uo = updateOffset.bind()
					.setUUID("offset", offset);

			return completedStatements(Arrays.asList(up, uo));
		});
		return eb.build();
	}

	@Override
	public CompletionStage<Optional<UUID>> prepare(CassandraSession db) {
		return db.executeCreateTable(
				"create table if not exists ratings (userId text, rating int, primary key (userId))")
				.thenCompose(done -> db.prepare("insert into ratings (userId, rating) values (?, ?)")
						.thenApply(ps -> {
							this.updatePain = ps;
							return done;
						}))
				.thenCompose(done -> db
						.executeCreateTable("create table if not exists offset (partition int, offset timeuuid, primary key (partition))"))
				.thenCompose(done -> db.prepare("insert into offset (partition, offset) values (1, ?)")
						.thenApply(ps -> {
							this.updateOffset = ps;
							return done;
						}))
				.thenCompose(done -> db.selectOne("select offset from offset"))
				.thenApply(opt -> opt.map(row -> row.getUUID("offset")));
	}

}
