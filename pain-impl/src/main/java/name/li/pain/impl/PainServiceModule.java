package name.li.pain.impl;

import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;

import name.li.pain.api.PainService;
import name.li.pain.users.api.UserService;
import name.li.pain.utils.CassandraOffsetSaverFactory;
import name.li.pain.utils.OffsetDaoFactory;


public class PainServiceModule extends AbstractModule implements ServiceGuiceSupport {
	
	@Override
	protected void configure() {
		bindServices(serviceBinding(PainService.class, PainServiceImpl.class));
		bindClient(UserService.class);
		bind(OffsetDaoFactory.class).to(CassandraOffsetSaverFactory.class);
	}
	
}
