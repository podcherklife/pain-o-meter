package name.li.pain.impl;

import java.time.Instant;
import java.util.Optional;

import com.lightbend.lagom.javadsl.persistence.PersistentEntity;

import akka.Done;
import name.li.pain.impl.commands.ChangePainRate;
import name.li.pain.impl.commands.GetPainRate;
import name.li.pain.impl.commands.PainMessage;
import name.li.pain.impl.events.PainEvent;
import name.li.pain.impl.events.PainRateChanged;

public class UserPainEntity extends PersistentEntity<PainMessage, PainEvent, PainState> {

	@Override
	public PersistentEntity<PainMessage, PainEvent, PainState>.Behavior initialBehavior(Optional<PainState> snapshot) {
		
		BehaviorBuilder b = newBehaviorBuilder(snapshot.orElse(null));
		
		b.setReadOnlyCommandHandler(GetPainRate.class, (event, ctx) -> ctx.reply(Optional.ofNullable(state())));
		
		b.setCommandHandler(ChangePainRate.class, 
				(command, ctx) -> ctx.thenPersist(
						new PainRateChanged(entityId(), command.getRate(), Instant.now()),
						event -> ctx.reply(Done.getInstance())));
		
		b.setEventHandler(PainRateChanged.class, event -> new PainState(event.getNewRate(), event.getTimestamp()));
		
		return b.build();
	}

}
