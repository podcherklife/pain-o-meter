package name.li.pain.impl;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRef;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraReadSide;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraSession;

import akka.Done;
import akka.NotUsed;
import akka.stream.javadsl.Source;
import name.li.pain.api.PainService;
import name.li.pain.api.UserRating;
import name.li.pain.api.UserRatingsStreamEvent;
import name.li.pain.impl.commands.ChangePainRate;
import name.li.pain.impl.commands.GetPainRate;
import name.li.pain.impl.commands.PainMessage;
import name.li.pain.users.api.UserService;
import name.li.pain.users.api.UserState;

public class PainServiceImpl implements PainService {

	private final PersistentEntityRegistry persistentEntityRegistry;
	private final UserService userService;
	private CassandraSession db;
	private RatingsStreamer ratingsStreamer;

	@Inject
	public PainServiceImpl(
			PersistentEntityRegistry persistentEntityRegistry,
			UserService userService,
			CassandraReadSide readSide,
			CassandraSession db,
			RatingsStreamer ratingsStreamer) {
		this.persistentEntityRegistry = persistentEntityRegistry;
		this.userService = userService;
		this.db = db;
		this.ratingsStreamer = ratingsStreamer;
		persistentEntityRegistry.register(UserPainEntity.class);
		readSide.register(PainRateEventProcessor.class);
	}

	@Override
	public ServiceCall<NotUsed, Done> rate(String userId, Integer value) {
		return req -> {
			checkUserExists(userId);

			PersistentEntityRef<PainMessage> ref = persistentEntityRegistry.refFor(UserPainEntity.class, userId);
			return ref.ask(new ChangePainRate(value));
		};
	}
	
	@Override
	public ServiceCall<NotUsed, Source<UserRatingsStreamEvent, NotUsed>> ratings(Optional<String> offset) {
		return ratingsStreamer.ratings(offset);
	}

	@Override
	public ServiceCall<NotUsed, Optional<UserRating>> getRating(String userId) {
		return req -> persistentEntityRegistry.refFor(UserPainEntity.class, userId)
				.ask(new GetPainRate())
				.thenApply(optRating -> optRating.map(r -> new UserRating(r.getCurrentPainRate(), userId, r.getTimeStamp())));
	}

	@Override
	public ServiceCall<NotUsed, Map<String, Integer>> getAllRatings() {
		return req -> {
			return db.selectAll("select * from ratings").thenApply(rows -> {
				return rows.stream().collect(Collectors.toMap(row -> row.getString("userId"), row -> row.getInt("rating")));
			});
		};
	}

	private void checkUserExists(String userId) {
		try {
			UserState user = userService.getUser(userId).invoke().toCompletableFuture().get();
			if (user == null) throw new RuntimeException("There is no such user =(");
		} catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException(e);
		}
	}
	

}
