package name.li.pain.impl;

import java.time.Instant;

public class PainState {
	
	private Integer currentPainRate;
	private Instant timeStamp;

	public PainState(Integer currentPainRate, Instant timeStamp) {
		this.currentPainRate = currentPainRate;
		this.timeStamp = timeStamp;
	}
	
	public Integer getCurrentPainRate() {
		return currentPainRate;
	}

	public Instant getTimeStamp() {
		return timeStamp;
	}

}
