package name.li.pain.impl.events;

import java.time.Instant;

public class PainRateChanged extends PainEvent {
	private static final long serialVersionUID = 1L;

	private Integer newRate;
	private Instant timestamp;

	public PainRateChanged(String entityId, Integer newRate, Instant timestamp) {
		super(entityId);
		this.newRate = newRate;
		this.timestamp = timestamp;
	}
	
	public Integer getNewRate() {
		return newRate;
	}
	
	public Instant getTimestamp() {
		return timestamp;
	}
	
}
