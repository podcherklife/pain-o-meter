package name.li.pain.impl.events;

import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.serialization.Jsonable;

public class PainEvent implements Jsonable, AggregateEvent<PainEvent> {
	private static final long serialVersionUID = 1L;

	public static AggregateEventTag<PainEvent> TAG = AggregateEventTag.of(PainEvent.class);

	@Override
	public AggregateEventTag<PainEvent> aggregateTag() {
		return TAG;
	}

	private String entityId;
	
	public PainEvent(String entityId) {
		super();
		this.entityId = entityId;
	}
	
	public String getEntityId() {
		return entityId;
	}
}
