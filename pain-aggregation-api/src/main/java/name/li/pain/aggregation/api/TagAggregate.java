package name.li.pain.aggregation.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TagAggregate {

	private Integer max;
	private Integer min;
	private Integer mean;
	private String name;

	public TagAggregate(
			@JsonProperty("max") Integer max,
			@JsonProperty("min") Integer min,
			@JsonProperty("mean") Integer mean,
			@JsonProperty("name") String name) {
		super();
		this.max = max;
		this.min = min;
		this.mean = mean;
		this.name = name;
	}

	public Integer getMax() {
		return max;
	}

	public Integer getMean() {
		return mean;
	}

	public Integer getMin() {
		return min;
	}
	
	public String getName() {
		return name;
	}

}
