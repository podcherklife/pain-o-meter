package name.li.pain.aggregation.api;

import java.util.Map;
import java.util.Set;

import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;

import akka.Done;
import akka.NotUsed;

public interface AggregationService extends Service {

	ServiceCall<NotUsed, TagAggregate> getAggregateForTag(String tag);

	ServiceCall<NotUsed, Map<String, TagAggregate>> getAggregatesForAllTags();

	ServiceCall<NotUsed, Done> rebuildAggregateForTag(String tag);

	ServiceCall<NotUsed, Set<String>> getUsersWithRatingsForTag(String tag);

	@Override
	default Descriptor descriptor() {
		return Service.named("pain-aggregation").withCalls(
				Service.pathCall("/api/aggregation/:tag", this::getAggregateForTag),
				Service.pathCall("/api/aggregation/:tag/rebuild", this::rebuildAggregateForTag),
				Service.pathCall("/api/aggregation/", this::getAggregatesForAllTags),
				Service.pathCall("/api/aggregation/:tag/users", this::getUsersWithRatingsForTag))
				.withAutoAcl(true);
	}
}
