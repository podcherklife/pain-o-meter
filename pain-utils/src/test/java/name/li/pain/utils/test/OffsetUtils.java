package name.li.pain.utils.test;

import org.apache.cassandra.utils.UUIDGen;

import com.lightbend.lagom.javadsl.persistence.Offset;

import name.li.pain.utils.OffsetStringifier;

public class OffsetUtils {
	
	public static class TestOffset {
		private Offset offset;
		
		public TestOffset(Offset offset) {
			this.offset = offset;
		}
		
		public Offset offset() {
			return offset;
		}
		
		public String string() {
			return OffsetStringifier.offsetToString(offset);
		}
		
	}
	
	public static TestOffset timeBased() {
		return new TestOffset(Offset.timeBasedUUID(UUIDGen.getTimeUUID()));
	}

}
