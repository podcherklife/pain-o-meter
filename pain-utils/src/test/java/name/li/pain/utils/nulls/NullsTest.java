package name.li.pain.utils.nulls;

import static org.junit.Assert.assertSame;

import org.junit.Test;

public class NullsTest {
	
	@Test(expected = IllegalStateException.class)
	public void checkShouldThrowExceptionIfNullIsPassed() {
		Object val = null;
		
		Nulls.check(val);
	}
	
	
	@Test
	public void checkShouldReturnPassedNotNonNullValue() {
		Object val = new Object();
		
		Object checkedVal = Nulls.check(val);
		
		assertSame(val, checkedVal);
	}

	@Test
	public void orShouldReturnFirstNotNullValue() {
		Object val1 = new Object(); 
		Object val2 = new Object();
		
		assertSame(null, Nulls.or(null, null));
		assertSame(val1, Nulls.or(null, val1));
		assertSame(val1, Nulls.or(val1, null));
		assertSame(val1, Nulls.or(val1, val2));
	}
}
