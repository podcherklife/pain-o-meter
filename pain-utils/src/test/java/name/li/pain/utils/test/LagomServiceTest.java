package name.li.pain.utils.test;

import static com.lightbend.lagom.javadsl.testkit.ServiceTest.startServer;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.Rule;
import org.junit.rules.TestName;

import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.testkit.ServiceTest;
import com.lightbend.lagom.javadsl.testkit.ServiceTest.Setup;
import com.lightbend.lagom.javadsl.testkit.ServiceTest.TestServer;

public abstract class LagomServiceTest {

	private static TestServer server;

	@Rule
	public TestName testName = new TestName();

	public static Setup defaultSetup() {
		return ServiceTest.defaultSetup().withCassandra(true);
	}

	public static void setup(Setup setup) {
		server = startServer(setup);
	}

	public static void tearDown() {
		server.stop();
	}

	public static <T> T get(CompletionStage<T> promise)
			throws InterruptedException, ExecutionException, TimeoutException {
		return promise.toCompletableFuture().get(30, TimeUnit.SECONDS);
	}

	public static <T, Q> ServiceCall<T, Q> call(Q res) {
		return (req) -> CompletableFuture.completedFuture(res);
	}
	
	public static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public void sleepFiveSeconds() {
		sleep(TimeUnit.SECONDS.toMillis(5));
	}

	public String buildIdForTest(String id) {
		return testName.getMethodName() + "-" + id;
	}
	
	public TestServer server() {
		return server;
	}
	
	public <T extends Service> T service(Class<T> serviceClass) {
		return server.client(serviceClass); 
	}
	
	public <T> T bean(Class<T> beanClass) {
		return server.injector().instanceOf(beanClass);
	}
}
