package name.li.pain.utils.nulls;

public class Nulls {
	
	public static <T> @NotNull T check(@Null T val, String errorMessage) {
		if (val == null) {
			throw new IllegalStateException(errorMessage);
		}
		return val;
	}
	
	public static <T> @NotNull T check(@Null T val) {
		return check(val, "Null check failed");
	}
	
	public static <T> T or(@NotNull T val1, @Null T val2) {
		if (val1 != null) {
			return val1;
		} else {
			return val2;
		}
	}

}
