package name.li.pain.utils;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import com.lightbend.lagom.javadsl.persistence.Offset;

import akka.Done;

public class InMemoryOffsetSaverFactory implements OffsetDaoFactory {

	@Override
	public OffsetDao prepare(String offsetId) {
		return new OffsetDao() {
			
			private Offset val;
			
			@Override
			public CompletionStage<Done> saveOffset(Offset offset) {
				val = offset;
				return CompletableFuture.completedFuture(Done.getInstance());
			}
			
			@Override
			public CompletionStage<Optional<Offset>> lastSavedOffset() {
				return CompletableFuture.completedFuture(Optional.ofNullable(val));
			}
		};
	}

}
