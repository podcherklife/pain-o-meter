package name.li.pain.utils;

import java.util.Optional;
import java.util.concurrent.CompletionStage;

import com.lightbend.lagom.javadsl.persistence.Offset;

import akka.Done;

public interface OffsetDaoFactory {
	
	public interface OffsetDao {
		CompletionStage<Optional<Offset>> lastSavedOffset();
		
		CompletionStage<Done> saveOffset(Offset offset);
		
	}
	
	
	OffsetDao prepare(String offsetId);
	
}
