package name.li.pain.utils;

import java.util.UUID;

import com.lightbend.lagom.javadsl.persistence.Offset;
import com.lightbend.lagom.javadsl.persistence.Offset.NoOffset;
import com.lightbend.lagom.javadsl.persistence.Offset.TimeBasedUUID;

public class OffsetStringifier {

	private static final String NO_OFFSET = "no_offset";

	public static String offsetToString(Offset offset) {
		if (offset instanceof TimeBasedUUID) {
			return offset.toString();
		} else if (offset instanceof NoOffset) {
			return NO_OFFSET;
		} else {
			throw new RuntimeException("Invalid offset type: " + offset.getClass());
		}
	}

	public static Offset stringToOffset(String string) {
		if (string.equals(NO_OFFSET)) {
			return Offset.NONE;
		} else {
			return Offset.timeBasedUUID(UUID.fromString(string));
		}
	}

}
