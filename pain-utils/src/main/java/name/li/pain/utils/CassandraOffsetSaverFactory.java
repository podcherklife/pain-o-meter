package name.li.pain.utils;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.lightbend.lagom.javadsl.persistence.Offset;
import com.lightbend.lagom.javadsl.persistence.Offset.NoOffset;
import com.lightbend.lagom.javadsl.persistence.Offset.TimeBasedUUID;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraSession;

import akka.Done;

@Singleton
public class CassandraOffsetSaverFactory implements OffsetDaoFactory {

	private final CassandraSession db;
	private final CompletionStage<PreparedStatement> insertQuery;
	private final CompletionStage<PreparedStatement> loadQuery;
	
	
	public class CassandraOffsetSaver implements OffsetDao {
		
		private final String offsetName;

		public CassandraOffsetSaver(String offsetName) {
			this.offsetName = offsetName;
		}

		@Override
		public CompletionStage<Optional<Offset>> lastSavedOffset() {
			return loadQuery.thenCompose(ps -> { 
				BoundStatement query = ps.bind();
				query.setString("offsetName", offsetName);
				return db.selectOne(query);
			})
			.thenApply(maybeRow -> maybeRow.map(row -> Offset.timeBasedUUID(row.getUUID("offset"))));
		}

		@Override
		public CompletionStage<Done> saveOffset(Offset offset) {
			if (offset instanceof TimeBasedUUID) {
				return insertQuery.thenCompose(ps -> {
					BoundStatement query = ps.bind();
					query.setString("offsetName", offsetName);
					query.setUUID("offset", ((TimeBasedUUID) offset).value());
					return db.executeWrite(query);
				});
			} else if (offset instanceof NoOffset) {
				// nobody cares then
				return CompletableFuture.completedFuture(Done.getInstance());
			} else {
				throw new IllegalArgumentException("Cannot save non UUID offset");
			}
		}
		
	}

	@Inject
	public CassandraOffsetSaverFactory(CassandraSession db) {
		this.db = db;
		CompletionStage<Done> prepareTables = db.executeCreateTable("create table if not exists savedOffsets (offsetName text, offset timeuuid, primary key(offsetName))");
		insertQuery = prepareTables.thenCompose(done -> db.prepare("insert into savedOffsets (offsetName, offset) values (?, ?)"));
		loadQuery = prepareTables.thenCompose(done -> db.prepare("select offset from savedOffsets where offsetName = ?"));
	}

	@Override
	public OffsetDao prepare(String offsetName) {
		return new CassandraOffsetSaver(offsetName);
	}

}
