scalaVersion in ThisBuild := "2.11.7"

val immutables = "org.immutables" % "value" % "2.4.3"

val mockito = "org.mockito" % "mockito-core" % "1.8.5" % "test"


lazy val painUtils = newProject("pain-utils")
  .settings(
    version := "1.0-SNAPSHOT",
    libraryDependencies ++= Seq(
      lagomJavadslPersistenceCassandra,
      lagomJavadslApi,
      lagomJavadslTestKit,
      mockito
    )
  )
  .settings(lagomForkedTestSettings: _*)

lazy val painApi = newProject("pain-api")
  .settings(
    version := "1.0-SNAPSHOT",
    libraryDependencies ++=Seq(
      lagomJavadslApi,
      immutables,
      mockito
    )
  )

lazy val painImpl = newProject("pain-impl")
  .enablePlugins(LagomJava)
  .settings(
    version := "1.0-SNAPSHOT",
    libraryDependencies ++= Seq(
      lagomJavadslPersistenceCassandra,
      lagomJavadslTestKit,
      mockito
    )
  )
  .settings(lagomForkedTestSettings: _*)
  .dependsOn(
    painApi,
    painUsersApi,
    painUtils % "compile->compile;test->test"
  )


lazy val painUsersApi = newProject("pain-users-api")
  .settings(
    version := "1.0-SNAPSHOT",
    libraryDependencies ++=Seq(
      lagomJavadslApi,
      immutables
    )
  )

lazy val painUsersImpl = newProject("pain-users-impl")
  .enablePlugins(LagomJava)
  .settings(
    version := "1.0-SNAPSHOT",
    libraryDependencies ++= Seq(
      lagomJavadslPersistenceCassandra,
      lagomJavadslTestKit,
      mockito
    )
  )
  .settings(lagomForkedTestSettings: _*)
  .dependsOn(
    painUsersApi,
    painUtils % "compile->compile;test->test"
  )


lazy val painAggregationApi = newProject("pain-aggregation-api")
  .settings(
    version := "1.0-SNAPSHOT",
    libraryDependencies ++=Seq(
      lagomJavadslApi,
      immutables
    )
  )

lazy val painAggregationImpl = newProject("pain-aggregation-impl")
  .enablePlugins(LagomJava)
  .settings(
    version := "1.0-SNAPSHOT",
    libraryDependencies ++= Seq(
      lagomJavadslPersistenceCassandra,
      lagomJavadslTestKit,
      mockito
    )
  )
  .settings(lagomForkedTestSettings: _*)
  .dependsOn(
    painUsersApi,
    painApi,
    painAggregationApi,
    painUtils % "compile->compile;test->test"
  )

lazy val webapp = newProject("webapp")
  .enablePlugins(PlayJava, LagomPlay)
  .settings(
    version := "1.0-SNAPSHOT",
    routesGenerator := InjectedRoutesGenerator,
    libraryDependencies ++= Seq(
      "org.webjars" % "jquery" % "2.2.0",
      "org.webjars" % "foundation" % "6.2.3",
      "com.lightbend.lagom" % "lagom-javadsl-play-integration_2.11" % "1.3.1",
      immutables,
      mockito
    )
  )
  .dependsOn(
    painApi,
    painUsersApi,
    painAggregationApi
  )

lazy val coverage = project

fork in jacoco.Config := true

def newProject(id: String) = Project(id, base = file(id))
  .settings(jacoco.settings: _*)
  .settings(
    fork in jacoco.Config := true
  )
  .settings(eclipseSettings: _*)
  .settings(javacOptions in compile ++= Seq(
    "-encoding", "UTF-8",
    "-source", "1.8",
    "-target", "1.8",
    "-Xlint:unchecked",
    "-Xlint:deprecation",
    "-parameters"))
  .settings(jacksonParameterNamesJavacSettings: _*) // applying it to every project even if not strictly needed.
  .settings(
    // change target directory for apt-generated sources
    javacOptions in Compile <++= target map { base =>
      val gen = base / "generated-sources"
      IO.createDirectory(gen) 
      Seq("-s", gen.getAbsolutePath)
    },
    managedSourceDirectories in Compile ++= Seq(target { _ / "generated-sources" }.value)
  )



// See https://github.com/FasterXML/jackson-module-parameter-names
lazy val jacksonParameterNamesJavacSettings = Seq(
  javacOptions in compile += "-parameters"
)

// Configuration of sbteclipse
// Needed for importing the project into Eclipse	
lazy val eclipseSettings = Seq(
  EclipseKeys.projectFlavor := EclipseProjectFlavor.Java,
  EclipseKeys.withBundledScalaContainers := false,
  EclipseKeys.createSrc := EclipseCreateSrc.Default + EclipseCreateSrc.Resource,
  //EclipseKeys.eclipseOutput := Some(".target"),
  EclipseKeys.withSource := true,
  EclipseKeys.withJavadoc := true,
  // avoid some scala specific source directories
  unmanagedSourceDirectories in Compile := Seq((javaSource in Compile).value),
  unmanagedSourceDirectories in Test := Seq((javaSource in Test).value)
)



