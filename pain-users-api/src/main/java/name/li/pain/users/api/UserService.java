package name.li.pain.users.api;

import static com.lightbend.lagom.javadsl.api.Service.named;
import static com.lightbend.lagom.javadsl.api.Service.pathCall;

import java.util.List;
import java.util.Set;

import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;

import akka.Done;
import akka.NotUsed;

public interface UserService extends Service {
	
	ServiceCall<NotUsed, String> newUser(String name);

	ServiceCall<NotUsed, Set<ImmutableTag>> getTags(String userId);

	ServiceCall<Set<ImmutableTag>, Done> updateTags(String userId);
	
	ServiceCall<NotUsed, List<UserState>> getAllUsers();
	
	ServiceCall<NotUsed, UserState> getUser(String userId);

	@Override
	default Descriptor descriptor() {
		return named("painusers").withCalls(
				pathCall("/api/users/create?name", this::newUser),
				pathCall("/api/users/all", this::getAllUsers),
				pathCall("/api/users/:userId/updateTags", this::updateTags),
				pathCall("/api/users/:userId/tags", this::getTags),
				pathCall("/api/users/:userId/", this::getUser))
				.withAutoAcl(true);
	}

}
