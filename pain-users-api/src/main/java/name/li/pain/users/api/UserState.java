package name.li.pain.users.api;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize
public class UserState {

	private String id;
	private String name;
	private Set<ImmutableTag> tags;

	@JsonCreator
	public UserState(
			@JsonProperty("id") String id,
			@JsonProperty("name") String name,
			@JsonProperty("tags") Set<ImmutableTag> tags) {
		super();
		this.id = id;
		this.name = name;
		this.tags = tags;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Set<ImmutableTag> getTags() {
		return tags;
	}

}
