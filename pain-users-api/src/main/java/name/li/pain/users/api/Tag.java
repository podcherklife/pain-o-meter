package name.li.pain.users.api;

import org.immutables.value.Value;
import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Immutable
@Value.Style(builder = "new")
@JsonDeserialize(builder = ImmutableTag.Builder.class)
public interface Tag {
	
	public abstract String name();

}
