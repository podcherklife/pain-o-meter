package helpers;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class TimeFormat {
	
	public static String formatInstantWithDefaultZoneId(Instant instant) {
		return ZonedDateTime.ofInstant(instant, ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
	}

}
