package controllers;

import java.util.HashSet;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;

import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Sink;
import name.li.pain.api.PainService;
import name.li.pain.api.UserRating;
import name.li.pain.users.api.ImmutableTag;
import name.li.pain.users.api.UserService;
import name.li.pain.users.api.UserState;
import play.libs.F.Either;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.WebSocket;

public class RootController extends Controller {

	private UserService userService;
	
	private PainService painService;

	private HttpExecutionContext ec;

	@Inject
	public RootController(UserService userService, PainService painService, HttpExecutionContext ec) {
		this.userService = userService;
		this.painService = painService;
		this.ec = ec;
	}

	public Result index() {
		return redirect(routes.RootController.listAllUsers());
	}

	public CompletionStage<Result> addUser(String userName) {
		return userService.newUser(userName).invoke().<Result>thenApplyAsync(done -> redirect(routes.RootController.listAllUsers()), ec.current());
	}

	public CompletionStage<Result> listAllUsers() {
		return userService.getAllUsers().invoke().<Result>thenApplyAsync(users -> {
			return ok(views.html.users.render(users));
		}, ec.current());
	}
	
	public CompletionStage<Result> showUser(String userId) {
		CompletionStage<Optional<UserRating>> ratingPromise = painService.getRating(userId).invoke();
		CompletionStage<UserState> userStatePromise = userService.getUser(userId).invoke();
		return ratingPromise.<UserState, Result>thenCombineAsync(userStatePromise, (rating, user) -> {
			return ok(views.html.user.render(user, rating.orElse(null)));
		}, ec.current());
		
	}
	
	public CompletionStage<Result> addUserTag(String userId, String tagName) {
		return userService.getUser(userId).invoke()
				.thenApply(user -> user.getTags())
				.thenCompose(existingTags -> {
					HashSet<ImmutableTag> allTags = new HashSet<>(existingTags);
					allTags.add(new ImmutableTag.Builder().name(tagName).build());
					return userService.updateTags(userId).invoke(allTags);
				})
				.<Result>thenApply(done -> redirect(routes.RootController.showUser(userId)));
	}
	
	
	public CompletionStage<Result> rate(String userId, Integer painRate) {
		return painService.rate(userId, painRate).invoke().thenApplyAsync(done -> redirect(routes.RootController.showUser(userId)), ec.current());
	}
	
	public WebSocket ratings() {
		return WebSocket.Json.acceptOrResult(req -> {
			return painService.ratings(Optional.empty()).invoke()
				.thenApply(source -> Either.Right(Flow.fromSinkAndSource(Sink.ignore(), source.map(r -> Json.toJson(r)))));
		});
	}

}
