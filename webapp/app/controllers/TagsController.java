package controllers;

import java.util.Set;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;

import name.li.pain.aggregation.api.AggregationService;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;

public class TagsController extends Controller {
	
	private AggregationService aggregationService;

	private HttpExecutionContext ec;

	@Inject
	public TagsController(AggregationService aggregationService, HttpExecutionContext ec) {
		super();
		this.aggregationService = aggregationService;
		this.ec = ec;
	}
	
	
	public CompletionStage<Result> showTag(String tagName) {
		CompletionStage<Set<String>> usersPromise = aggregationService.getUsersWithRatingsForTag(tagName).invoke();
		return aggregationService.getAggregateForTag(tagName).invoke()
				.thenCombineAsync(usersPromise, (tag, users) -> ok(views.html.tag.render(tag, users)), ec.current());
	}

}
