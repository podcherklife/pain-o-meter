package name.li.pain.web;

import javax.inject.Singleton;

import com.google.inject.AbstractModule;
import com.lightbend.lagom.internal.javadsl.BinderAccessor;
import com.lightbend.lagom.internal.javadsl.client.ServiceClientProvider;
import com.lightbend.lagom.javadsl.api.Service;

import name.li.pain.aggregation.api.AggregationService;
import name.li.pain.api.PainService;
import name.li.pain.users.api.UserService;

public class LagomModule extends AbstractModule {

	@Override
	protected void configure() {
		bindClient(UserService.class);
		bindClient(PainService.class);
		bindClient(AggregationService.class);
	}

	private <T extends Service> void bindClient(Class<T> service) {
		BinderAccessor.binder(this).bind(service)
				.toProvider(new ServiceClientProvider<T>(service))
				.in(Singleton.class);
	}

}
