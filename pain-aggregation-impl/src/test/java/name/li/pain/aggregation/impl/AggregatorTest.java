package name.li.pain.aggregation.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.util.Arrays;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Sets;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;
import com.lightbend.lagom.javadsl.testkit.ServiceTest;

import akka.actor.ActorSystem;
import akka.stream.javadsl.Source;
import name.li.pain.api.ImmutableUserRatingsStreamEvent;
import name.li.pain.api.UserRating;
import name.li.pain.users.api.ImmutableTag;
import name.li.pain.users.api.UserService;
import name.li.pain.users.api.UserState;
import name.li.pain.utils.OffsetDaoFactory;
import name.li.pain.utils.OffsetDaoFactory.OffsetDao;
import name.li.pain.utils.test.LagomServiceTest;
import name.li.pain.utils.test.OffsetUtils;
import name.li.pain.utils.test.OffsetUtils.TestOffset;

@RunWith(MockitoJUnitRunner.class)
public class AggregatorTest extends LagomServiceTest {
	
	
	@BeforeClass
	public static void start() {
		setup(defaultSetup()
				.withCassandra(true)
				.configureBuilder(b -> b.overrides(
						ServiceTest.bind(AggregatorManager.class).toInstance(noopAggregatorManager()), 
						ServiceTest.bind(UserService.class).toInstance(fakeUserService())
						)));
	}

	private static ImmutableTag tag1 = new ImmutableTag.Builder().name("tag1").build();
	private static ImmutableTag tag2 = new ImmutableTag.Builder().name("tag2").build();
	
	private static UserState user1 = new UserState("user1Id", "user1", Sets.newHashSet(tag1));
	private static UserState user2 = new UserState("user1Id", "user1", Sets.newHashSet(tag2));
	
	private static UserService fakeUserService() {
		UserService us = Mockito.mock(UserService.class);
		when(us.getUser(user1.getId())).thenReturn(call(user1));
		when(us.getTags(user1.getId())).thenReturn(call(user1.getTags()));
		when(us.getUser(user2.getId())).thenReturn(call(user2));
		when(us.getTags(user2.getId())).thenReturn(call(user2.getTags()));
		return us;
	}
	
	private static AggregatorManager noopAggregatorManager() {
		return mock(AggregatorManager.class);
	}

	@AfterClass
	public static void stop() {
		tearDown();
	}
	
	@Mock
	public OffsetDaoFactory offsetDaoFactory;
	
	@Mock
	public OffsetDao offsetDao;
	
	@Before
	public void initOffsetDaoMocks() {
		when(offsetDaoFactory.prepare(Mockito.anyString())).thenReturn(offsetDao);
	}
	
	@Test
	public void shouldStoreOffsetForEachHandledEvent() {
		PersistentEntityRegistry registry = bean(PersistentEntityRegistry.class);
		UserService userService = service(UserService.class);

		Aggregator aggregator = new Aggregator(ActorSystem.create(), registry, userService, offsetDaoFactory);

		TestOffset offset1 = OffsetUtils.timeBased();
		TestOffset offset2 = OffsetUtils.timeBased();
		TestOffset offset3 = OffsetUtils.timeBased();

		aggregator.consume(Source.from(Arrays.asList(
				ImmutableUserRatingsStreamEvent.builder()
					.rating(new UserRating(1, user1.getId(), Instant.now()))
					.offset(offset1.string())
					.build(),
				ImmutableUserRatingsStreamEvent.builder()
					.rating(new UserRating(2, user1.getId(), Instant.now()))
					.offset(offset2.string())
					.build(),
				ImmutableUserRatingsStreamEvent.builder()
					.rating(new UserRating(3, user2.getId(), Instant.now()))
					.offset(offset3.string())
					.build()
				)));
		
		// wait aggregator to consume events
		sleepFiveSeconds();
		
		verify(offsetDao).saveOffset(offset1.offset());
		verify(offsetDao).saveOffset(offset2.offset());
		verify(offsetDao).saveOffset(offset3.offset());
	}

}
