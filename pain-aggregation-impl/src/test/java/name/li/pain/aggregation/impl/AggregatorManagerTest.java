package name.li.pain.aggregation.impl;

import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import org.apache.cassandra.utils.UUIDGen;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.persistence.Offset;

import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import akka.stream.KillSwitch;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import name.li.pain.aggregation.impl.AggregatorManager.AggregationControl;
import name.li.pain.api.ImmutableUserRatingsStreamEvent;
import name.li.pain.api.PainService;
import name.li.pain.api.UserRating;
import name.li.pain.api.UserRatingsStreamEvent;
import name.li.pain.users.api.UserService;
import name.li.pain.utils.OffsetStringifier;

@RunWith(MockitoJUnitRunner.class)
public class AggregatorManagerTest {

	
	@Mock
	private PainService ps;
	
	@Mock
	private UserService us;
	
	@Mock
	private Aggregator aggregator;
	
	@Test
	public void shouldRetryConnectingAfterInitialException() throws InterruptedException {
		// service always throws exception
		doThrow(new RuntimeException()).when(ps).ratings(Mockito.any());
		
		//start process
		AggregationControl control = setupAggregatorWithSource(null);

		//wait for about two tries
		Thread.sleep(TimeUnit.SECONDS.toMillis(3));
		
		// method should have been at least twice
		verify(ps, atLeast(2)).ratings(Mockito.any());
		
		control.stop();
	}
	
	@Test
	public void shouldRecoverWhenStreamCompletes() throws InterruptedException {
		AggregationControl control = setupAggregatorWithSource(Source.single(
				ImmutableUserRatingsStreamEvent.builder()
					.rating(new UserRating(1, "user", Instant.now()))
					.offset("")
					.build()
				));
		
		//wait for retry
		Thread.sleep(TimeUnit.SECONDS.toMillis(3));
		
		// methods should have been called at least twice
		verify(ps, atLeast(2)).ratings(Mockito.any());
		verify(aggregator, atLeast(2)).consume(Mockito.any());
		
		control.stop();
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void shouldUseOffsetProvidedByAggregator() throws InterruptedException {
		Offset offset1 = Offset.timeBasedUUID(UUIDGen.getTimeUUID());
		Offset offset2 = Offset.timeBasedUUID(UUIDGen.getTimeUUID());

		AggregationControl control = setupAggregatorWithSource(Source.from(Arrays.asList(
				ImmutableUserRatingsStreamEvent.builder()
					.offset(OffsetStringifier.offsetToString(offset1))
					.rating(new UserRating(1, "user1", Instant.now()))
					.build(),
				ImmutableUserRatingsStreamEvent.builder()
					.offset(OffsetStringifier.offsetToString(offset2))
					.rating(new UserRating(1, "user1", Instant.now()))
					.build()
				)));
		
		
		// XXX: hack - this return overrides behavior set in setupAggregatorWithSource
		// but by the time when behavior is overwritten, reconnecting thread in AggregationManager
		// may have already completed first connection attempt
		when(aggregator.getLastEventOffset()).thenReturn(
				CompletableFuture.completedFuture(Offset.NONE),
				CompletableFuture.completedFuture(offset2));

		//wait for a couple of reconnects
		Thread.sleep(TimeUnit.SECONDS.toMillis(5));
		
		verify(ps, atLeast(1)).ratings(Optional.of(OffsetStringifier.offsetToString(Offset.NONE))); // initial try, no offset
		verify(ps, atLeast(1)).ratings(Optional.of(OffsetStringifier.offsetToString(offset2))); // reconnect, last offset
		
		control.stop();
	}
	
	private AggregationControl setupAggregatorWithSource(Source<UserRatingsStreamEvent, NotUsed> source) {
		AggregationControl control = new AggregationControl();
		when(aggregator.getLastEventOffset()).thenReturn(CompletableFuture.completedFuture(Offset.NONE));
		ServiceCall<NotUsed, Source<UserRatingsStreamEvent, NotUsed>> ratingsMethod = req -> CompletableFuture.completedFuture(Source.empty());
		if (source != null) {
			doReturn(ratingsMethod).when(ps).ratings(Mockito.any());
			doAnswer(i -> {
				@SuppressWarnings("unchecked")
				Source<UserRatingsStreamEvent, NotUsed> src = ((Source<UserRatingsStreamEvent, NotUsed>) i.getArguments()[0]);
				src.runWith(Sink.ignore(), ActorMaterializer.create(ActorSystem.create()));
				return new KillSwitch() {
					@Override
					public void shutdown() {
					}
					
					@Override
					public void abort(Throwable ex) {
					}
				};
			}).when(aggregator).consume(Mockito.any());
		}
		
		new AggregatorManager(ps, aggregator, control, TimeUnit.SECONDS.toMillis(1));
		return control;
	}
	

}
