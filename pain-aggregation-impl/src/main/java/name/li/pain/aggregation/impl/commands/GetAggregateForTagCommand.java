package name.li.pain.aggregation.impl.commands;

import name.li.pain.aggregation.api.TagAggregate;

public class GetAggregateForTagCommand extends TagAggregationCommand<TagAggregate> {

	public GetAggregateForTagCommand(String tagName) {
		super(tagName);
	}

}
