package name.li.pain.aggregation.impl;

import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;

import akka.Done;
import akka.NotUsed;
import name.li.pain.aggregation.api.AggregationService;
import name.li.pain.aggregation.api.TagAggregate;
import name.li.pain.aggregation.impl.commands.GetAggregateForTagCommand;
import name.li.pain.aggregation.impl.commands.GetUsersWithRatingsForTag;
import name.li.pain.aggregation.impl.commands.RebuildAggregateCommand;
import name.li.pain.api.PainService;

public class AggregationServiceImpl implements AggregationService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final PersistentEntityRegistry registry;
	private final PainService ratingsService;

	@Inject
	public AggregationServiceImpl(PersistentEntityRegistry registry, PainService ratingsService, Aggregator aggr) {
		this.registry = registry;
		this.ratingsService = ratingsService;
		registry.register(TagAggregationEntity.class);
	}

	@Override
	public ServiceCall<NotUsed, TagAggregate> getAggregateForTag(String tag) {
		return req -> registry.refFor(TagAggregationEntity.class, tag)
				.ask(new GetAggregateForTagCommand(tag));
	}

	@Override
	public ServiceCall<NotUsed, Map<String, TagAggregate>> getAggregatesForAllTags() {
		return req -> ratingsService.getAllRatings().invoke().thenApply(allRatings -> {
			logger.info("Got response from other service: {}", allRatings);
			throw new IllegalStateException("Not implemented! But other service call was successful.");
		});
	}

	@Override
	public ServiceCall<NotUsed, Done> rebuildAggregateForTag(String tag) {
		return req -> registry.refFor(TagAggregationEntity.class, tag).ask(new RebuildAggregateCommand(tag));
	}

	@Override
	public ServiceCall<NotUsed, Set<String>> getUsersWithRatingsForTag(String tag) {
		return req -> registry.refFor(TagAggregationEntity.class, tag).ask(new GetUsersWithRatingsForTag(tag));
	}

}
