package name.li.pain.aggregation.impl.events;

public class RatingValueAdded implements TagAggregationEvent {

	private static final long serialVersionUID = 1L;
	private String tag;
	private Integer value;
	private String userId;

	public RatingValueAdded(String tag, Integer value, String userId) {
		this.tag = tag;
		this.value = value;
		this.userId = userId;
	}

	public String getTag() {
		return tag;
	}

	public String getUserId() {
		return userId;
	}

	public Integer getValue() {
		return value;
	}

}
