package name.li.pain.aggregation.impl;

import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;

import name.li.pain.aggregation.api.AggregationService;
import name.li.pain.api.PainService;
import name.li.pain.users.api.UserService;
import name.li.pain.utils.CassandraOffsetSaverFactory;
import name.li.pain.utils.OffsetDaoFactory;


public class AggregationServiceModule extends AbstractModule implements ServiceGuiceSupport {
	
	@Override
	protected void configure() {
		bindServices(serviceBinding(AggregationService.class, AggregationServiceImpl.class));
		bindClient(PainService.class);
		bindClient(UserService.class);
		bind(AggregatorManager.class).asEagerSingleton();
		bind(OffsetDaoFactory.class).to(CassandraOffsetSaverFactory.class);
	}
	
}
