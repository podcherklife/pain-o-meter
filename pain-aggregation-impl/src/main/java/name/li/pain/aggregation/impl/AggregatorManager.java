package name.li.pain.aggregation.impl;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.dispatch.Recover;
import akka.stream.Graph;
import akka.stream.KillSwitch;
import akka.stream.SourceShape;
import akka.stream.actor.ActorSubscriberMessage;
import akka.stream.actor.OneByOneRequestStrategy;
import akka.stream.actor.RequestStrategy;
import akka.stream.actor.UntypedActorSubscriber;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import name.li.pain.api.PainService;
import name.li.pain.api.UserRatingsStreamEvent;
import name.li.pain.utils.OffsetStringifier;

public class AggregatorManager {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public static class AggregationControl {
		CompletableFuture<?> promise = new CompletableFuture<>();

		public void stop() {
			promise.completeExceptionally(createCancellationException());
		}

		public void onStop(Runnable callback) {
			onStop(e -> callback.run());
		}

		public void onStop(Consumer<Throwable> consumer) {
			promise.exceptionally(e -> {
				consumer.accept(createCancellationException());
				return null;
			});
		}

		private Exception createCancellationException() {
			return new RuntimeException("Aggregation stop requested");
		}
	}

	static class CompleteListenerActor extends UntypedActorSubscriber {

		private Runnable callback;

		public CompleteListenerActor(Runnable callback) {
			this.callback = callback;
		}

		@Override
		public void onReceive(Object message) throws Throwable {
			if (message == ActorSubscriberMessage.onCompleteInstance()) {
				callback.run();
			}
		}

		@Override
		public RequestStrategy requestStrategy() {
			return OneByOneRequestStrategy.getInstance();
		}

	}

	private PainService ratingsService;

	private volatile Aggregator aggregator;

	private AggregationControl control;

	private final long timeout;

	@Inject
	public AggregatorManager(PainService ratingsService, Aggregator aggregator) {
		this(ratingsService, aggregator, new AggregationControl(), TimeUnit.SECONDS.toMillis(10));
	}
	
	public AggregatorManager(PainService ratingsService, Aggregator aggregator, AggregationControl control, long timeout) {
		super();
		this.ratingsService = ratingsService;
		this.control = new AggregationControl();
		this.aggregator = aggregator;
		this.timeout = timeout;
		setupAggregator(control);
	}

	public void setupAggregator(AggregationControl control) {
		getRatingsStreamPromise(control)
				.thenApply(stream -> {
					return stream.recoverWith(recoverStream(control));
				})
				.thenApply(this::addOnCompleteListener)
				.thenAccept(stream -> {
					KillSwitch killSwitch = aggregator.consume(stream);
					control.onStop(() -> killSwitch.abort(new RuntimeException("Aggregation aborted")));
				})
				.exceptionally(e -> {
					logger.error("Failed to get ratings stream", e);
					return null;
				});
	}

	public Source<UserRatingsStreamEvent, NotUsed> addOnCompleteListener(Source<UserRatingsStreamEvent, NotUsed> stream) {
		Runnable onComplete = () -> {
			logger.info("Ratings stream completed, restoring");
			try {
				Thread.sleep(getTimeout());
			} catch (InterruptedException e) {
				throw new RuntimeException();
			}
			setupAggregator(control);
		};
		Sink<UserRatingsStreamEvent, ActorRef> notifyOnComplete = Sink.actorSubscriber(Props.create(CompleteListenerActor.class, onComplete));
		return stream.alsoTo(notifyOnComplete);
	}

	// for some reason compiler says that Recover is not a SAM
	private Recover<Graph<SourceShape<UserRatingsStreamEvent>, NotUsed>> recoverStream(AggregationControl control) {
		return new Recover<Graph<SourceShape<UserRatingsStreamEvent>, NotUsed>>() {
			@Override
			public Graph<SourceShape<UserRatingsStreamEvent>, NotUsed> recover(Throwable error) throws Throwable {
				logger.error("Ratings stream failed, recovering", error);
				Thread.sleep(getTimeout());
				try {
					Source<UserRatingsStreamEvent, NotUsed> stream = getRatingsStream(control);
					logger.error("Successfully recovered ratings stream");
					return stream;
				} catch (Exception e) {
					logger.error("Failed to recover stream", e);
					throw e;
				}
			};
		};
	}

	public CompletableFuture<Source<UserRatingsStreamEvent, NotUsed>> getRatingsStreamPromise(AggregationControl control) {
		CompletableFuture<Source<UserRatingsStreamEvent, NotUsed>> ratingsPromise = new CompletableFuture<>();
		new Thread(() -> {
			try {
				Source<UserRatingsStreamEvent, NotUsed> stream = getRatingsStream(control);
				ratingsPromise.complete(stream);
			} catch (Exception e) {
				ratingsPromise.completeExceptionally(e);
			}
		}).start();
		return ratingsPromise;
	}

	public Source<UserRatingsStreamEvent, NotUsed> getRatingsStream(AggregationControl control) {
		AtomicBoolean cancellationSwitch = new AtomicBoolean(false);
		control.onStop(() -> cancellationSwitch.set(true));
		Consumer<AtomicBoolean> checkCancelled = flag -> {
			if (flag.get()) {
				throw new RuntimeException("Connection cancelled");
			}
		};

		// XXX: when lagom hot-reloads code and restarts service, this thread will keep 
		// running forever unless we limit the number of retries
		int retries = 0;
		while (retries < 5) {
			retries += 1;
			checkCancelled.accept(cancellationSwitch);
			try {
				Source<UserRatingsStreamEvent, NotUsed> stream = aggregator
						.getLastEventOffset()
						.thenCompose(offset -> ratingsService.ratings(Optional.of(offset).map(OffsetStringifier::offsetToString)).invoke())
						.toCompletableFuture().get();
				logger.info("Got ratings stream");
				return stream;
			} catch (InterruptedException e) {
				throw new RuntimeException("Interrupted", e);
			} catch (Exception e) {
				try {
					Thread.sleep(getTimeout());
				} catch (InterruptedException e1) {
					throw new RuntimeException("Interrupted", e);
				}
				logger.error("Failed to get ratings stream, retrying", e);
			}
		}
		throw new RuntimeException("Failed to get ratings stream after " + retries  + " tries");
	}

	private long getTimeout() {
		return timeout;
	}

}
