package name.li.pain.aggregation.impl.commands;

import akka.Done;

public class RebuildAggregateCommand extends TagAggregationCommand<Done> {

	public RebuildAggregateCommand(String tagName) {
		super(tagName);
	}

}
