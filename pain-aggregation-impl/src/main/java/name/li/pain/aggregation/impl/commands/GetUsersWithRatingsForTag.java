package name.li.pain.aggregation.impl.commands;

import java.util.Set;

public class GetUsersWithRatingsForTag extends TagAggregationCommand<Set<String>> {
	
	public GetUsersWithRatingsForTag(String tagName) {
		super(tagName);
	}

}
