package name.li.pain.aggregation.impl;

import java.util.Set;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lightbend.lagom.javadsl.persistence.Offset;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRef;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;

import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import akka.stream.ActorMaterializerSettings;
import akka.stream.KillSwitch;
import akka.stream.KillSwitches;
import akka.stream.Supervision;
import akka.stream.UniqueKillSwitch;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import name.li.pain.aggregation.impl.commands.AddTagRating;
import name.li.pain.aggregation.impl.commands.TagAggregationCommand;
import name.li.pain.api.UserRating;
import name.li.pain.api.UserRatingsStreamEvent;
import name.li.pain.users.api.ImmutableTag;
import name.li.pain.users.api.UserService;
import name.li.pain.utils.OffsetDaoFactory;
import name.li.pain.utils.OffsetDaoFactory.OffsetDao;
import name.li.pain.utils.OffsetStringifier;

public class Aggregator {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private ActorSystem as;
	private PersistentEntityRegistry registry;
	private UserService userService;
	private OffsetDao offsetDao;

	@Inject
	public Aggregator(
			ActorSystem as,
			PersistentEntityRegistry registry,
			UserService userService,
			OffsetDaoFactory offsetDaoFactory) {
		this.as = as;
		this.registry = registry;
		this.userService = userService;
		this.offsetDao = offsetDaoFactory.prepare("aggregatorOffset");
	}

	public KillSwitch consume(Source<UserRatingsStreamEvent, NotUsed> ratings) {

		ActorMaterializerSettings settings = ActorMaterializerSettings.create(as).withSupervisionStrategy(th -> {
			logger.error("Failed to handle rating event: {}", th);
			return Supervision.stop();
		});

		Source<UserRatingsStreamEvent, UniqueKillSwitch> preparedRatings = ratings
				.map(ratingEvent -> {
					UserRating rating = ratingEvent.rating();
					CompletionStage<Set<ImmutableTag>> tagsPromise = userService.getTags(rating.getUserId()).invoke();
					tagsPromise.thenAccept(tags -> {
						tags.stream().forEach(tag -> {
							PersistentEntityRef<TagAggregationCommand<?>> tagAggregate = registry
									.refFor(TagAggregationEntity.class, tag.name());
							tagAggregate.ask(new AddTagRating(tag.name(), rating.getUserId(), rating.getValue()));
						});
					});
					return ratingEvent;
				})
				.alsoTo(Sink.foreach(event -> updateOffset(OffsetStringifier.stringToOffset(event.offset()))))
				.viaMat(KillSwitches.single(), Keep.right());
		return preparedRatings
				.to(Sink.ignore())
				.run(ActorMaterializer.create(settings, as));
	}

	public void updateOffset(Offset offset) {
		offsetDao.saveOffset(offset);
	}

	public CompletionStage<Offset> getLastEventOffset() {
		return offsetDao.lastSavedOffset().thenApply(mayBeOffset -> mayBeOffset.orElse(Offset.NONE));
	}

}
