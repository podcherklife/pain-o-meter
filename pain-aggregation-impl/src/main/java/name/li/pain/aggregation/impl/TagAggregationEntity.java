package name.li.pain.aggregation.impl;

import static java.util.Collections.emptySet;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lightbend.lagom.javadsl.persistence.PersistentEntity;

import akka.Done;
import name.li.pain.aggregation.api.TagAggregate;
import name.li.pain.aggregation.impl.commands.AddTagRating;
import name.li.pain.aggregation.impl.commands.GetAggregateForTagCommand;
import name.li.pain.aggregation.impl.commands.GetUsersWithRatingsForTag;
import name.li.pain.aggregation.impl.commands.RebuildAggregateCommand;
import name.li.pain.aggregation.impl.commands.TagAggregationCommand;
import name.li.pain.aggregation.impl.events.AggregationRebuiltRequestedEvent;
import name.li.pain.aggregation.impl.events.RatingValueAdded;
import name.li.pain.aggregation.impl.events.TagAggregationEvent;
import name.li.pain.users.api.ImmutableTag;
import name.li.pain.utils.nulls.Nulls;

public class TagAggregationEntity extends PersistentEntity<TagAggregationCommand<?>, TagAggregationEvent, TagAggregationState> {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public PersistentEntity<TagAggregationCommand<?>, TagAggregationEvent, TagAggregationState>.Behavior initialBehavior(Optional<TagAggregationState> snapshot) {
		PersistentEntity<TagAggregationCommand<?>, TagAggregationEvent, TagAggregationState>.BehaviorBuilder bb =
				newBehaviorBuilder(snapshot.orElse(null));
		
		bb.setCommandHandler(RebuildAggregateCommand.class, (command, ctx) -> {
			return ctx.thenPersist(new AggregationRebuiltRequestedEvent(), (evt) -> ctx.reply(Done.getInstance()));
		});
		
		bb.setCommandHandler(AddTagRating.class, (command, ctx) -> {
			return ctx.thenPersist(new RatingValueAdded(command.getTagName(), command.getValue(), command.getUserId()), evt -> ctx.reply(Done.getInstance()));
		});
		
		bb.setEventHandler(RatingValueAdded.class, evt -> {
			return updateAggregateState(Nulls.or(state(), initialStateForFolding(entityId())), evt);
		});
		
		bb.setEventHandler(AggregationRebuiltRequestedEvent.class, evt -> {
			// FIXME: not implemented, just a proof of concept
			logger.info("Rebuilding aggregate for {}", entityId());
			return state();
		});
		
		bb.setReadOnlyCommandHandler(GetAggregateForTagCommand.class, (command, ctx) -> {
			ctx.reply(Optional.ofNullable(state())
					.map(state -> new TagAggregate(
							state.max(),
							state.min(),
							state.mean(),
							state.tag().name()))
					.orElse(tagAggregateForNoRatingsState()));
		});
		
		bb.setReadOnlyCommandHandler(GetUsersWithRatingsForTag.class, (command, ctx) -> {
			ctx.reply(Optional.ofNullable(state()).map(e -> e.users()).orElse(emptySet()));
		});
		
		return bb.build();
	}
	
	private TagAggregate tagAggregateForNoRatingsState() {
		return new TagAggregate(0, 0, 0, entityId());
	}
	
	private TagAggregationState initialStateForFolding(String tagName) {
		return new ImmutableTagAggregationState.Builder()
				.min(Integer.MAX_VALUE)
				.tag(new ImmutableTag.Builder().name(tagName).build())
				.max(Integer.MIN_VALUE)
				.count(0)
				.sum(0)
				.build();
	}
	
	private TagAggregationState updateAggregateState(
			TagAggregationState currentState,
			RatingValueAdded ratingValueAddedEvent) {
		Integer ratingValue = ratingValueAddedEvent.getValue();
		return new ImmutableTagAggregationState.Builder().from(currentState)
			.min(Math.min(currentState.min(), ratingValue)) 
			.max(Math.max(currentState.max(), ratingValue))
			.count(currentState.count() + 1)
			.sum(currentState.sum() + ratingValue)
			.addUsers(ratingValueAddedEvent.getUserId())
			.build();
	}

}
