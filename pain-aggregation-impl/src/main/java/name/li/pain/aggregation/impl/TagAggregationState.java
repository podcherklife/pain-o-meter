package name.li.pain.aggregation.impl;

import java.util.Set;

import org.immutables.value.Value;
import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import name.li.pain.users.api.Tag;

@Immutable
@Value.Style(builder = "new")
@JsonDeserialize(builder = ImmutableTagAggregationState.Builder.class)
public interface TagAggregationState {

	Tag tag();

	Set<String> users();

	@Value.Lazy
	default Integer mean() {
		Integer count = count();
		return count == 0 ? 0 : (sum() / count());
	}

	Integer count();
	
	Integer min();
	
	Integer max();
	
	Integer sum();

}