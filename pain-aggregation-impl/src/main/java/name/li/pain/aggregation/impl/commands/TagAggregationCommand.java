package name.li.pain.aggregation.impl.commands;

import com.lightbend.lagom.javadsl.persistence.PersistentEntity;

public class TagAggregationCommand<R> implements PersistentEntity.ReplyType<R> {
	private String tagName;
	
	public TagAggregationCommand(String tagName) {
		this.tagName = tagName;
	}
	
	public String getTagName() {
		return tagName;
	}
}
