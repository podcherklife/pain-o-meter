package name.li.pain.aggregation.impl.commands;

import akka.Done;

public class AddTagRating extends TagAggregationCommand<Done> {

	private String userId;
	private int value;

	public AddTagRating(String tagName, String userId, int value) {
		super(tagName);
		this.userId = userId;
		this.value = value;
	}

	public String getUserId() {
		return userId;
	}

	public int getValue() {
		return value;
	}

}
