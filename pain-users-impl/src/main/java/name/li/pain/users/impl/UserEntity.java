package name.li.pain.users.impl;

import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import com.lightbend.lagom.javadsl.persistence.PersistentEntity;

import akka.Done;
import name.li.pain.users.impl.commands.CreatePainUserCommand;
import name.li.pain.users.impl.commands.GetUserState;
import name.li.pain.users.impl.commands.GetUserTags;
import name.li.pain.users.impl.commands.UpdateTagsCommand;
import name.li.pain.users.impl.commands.UserCommand;
import name.li.pain.users.impl.events.TagsUpdatedEvent;
import name.li.pain.users.impl.events.UserCreatedEvent;
import name.li.pain.users.impl.events.UserEvent;

public class UserEntity extends PersistentEntity<UserCommand<?>, UserEvent, UserState> {

	@Override
	public Behavior initialBehavior(Optional<UserState> snapshot) {
		return snapshot.map(this::normalBehavior).orElse(notCreatedBehavior());
	}

	public Behavior normalBehavior(UserState initialState) {
		BehaviorBuilder bb = newBehaviorBuilder(initialState);

		bb.setCommandHandler(UpdateTagsCommand.class,
				(command, ctx) -> ctx.thenPersist(
						new TagsUpdatedEvent(entityId(), command.getTags()),
						(state) -> ctx.reply(Done.getInstance())));
		bb.setEventHandler(TagsUpdatedEvent.class, evt -> new UserState(state().getCreationDate(), evt.getTags()));

		bb.setReadOnlyCommandHandler(GetUserTags.class,
				(command, ctx) -> ctx.reply(state().getTags()));
		bb.setReadOnlyCommandHandler(GetUserState.class,
				(command, ctx) -> ctx.reply(new name.li.pain.users.api.UserState(entityId(), entityId(), state().getTags())));
		bb.setReadOnlyCommandHandler(CreatePainUserCommand.class,
				(command, ctx) -> ctx.invalidCommand("User already exists"));

		return bb.build();
	}

	public Behavior notCreatedBehavior() {
		BehaviorBuilder bb = newBehaviorBuilder(null);
		bb.setEventHandlerChangingBehavior(UserCreatedEvent.class,
				evt -> normalBehavior(new UserState(getCurrentDate(), Collections.emptySet())));

		bb.setCommandHandler(CreatePainUserCommand.class,
				(command, ctx) -> {
					if (!command.getName().equals(entityId())) 
						ctx.commandFailed(new IllegalArgumentException("Username and entityId are not the same"));
					
					return ctx.thenPersist(
							new UserCreatedEvent(command.getName(), entityId()),
							(evt) -> ctx.reply(entityId()));
				});

		bb.setReadOnlyCommandHandler(UpdateTagsCommand.class,
				(command, ctx) -> ctx.invalidCommand("User does not exist"));
		bb.setReadOnlyCommandHandler(GetUserTags.class,
				(command, ctx) -> ctx.invalidCommand("User does not exist"));
		bb.setReadOnlyCommandHandler(GetUserState.class,
				(command, ctx) -> ctx.invalidCommand("User does not exist"));

		return bb.build();
	}

	public Date getCurrentDate() {
		return new Date();
	}

}
