package name.li.pain.users.impl.commands;

import java.util.Set;

import akka.Done;
import name.li.pain.users.api.ImmutableTag;

public class UpdateTagsCommand implements UserCommand<Done> {

	private Set<ImmutableTag> tags;

	public UpdateTagsCommand(Set<ImmutableTag> tags) {
		super();
		this.tags = tags;
	}

	public Set<ImmutableTag> getTags() {
		return tags;
	}

}
