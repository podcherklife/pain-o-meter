package name.li.pain.users.impl.events;

public class UserCreatedEvent extends UserEvent {
	private static final long serialVersionUID = 1L;
	
	private String name;
	
	public UserCreatedEvent(String name, String userId) {
		super(userId);
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
