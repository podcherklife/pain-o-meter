package name.li.pain.users.impl.events;

import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.serialization.Jsonable;

public abstract class UserEvent implements Jsonable, AggregateEvent<UserEvent> {
	private static final long serialVersionUID = 1L;

	public static AggregateEventTag<UserEvent> TAG = AggregateEventTag.of(UserEvent.class);

	private String userId;
	
	public UserEvent(String userId) {
		this.userId = userId;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public AggregateEventTag<UserEvent> aggregateTag() {
		return TAG;
	}
}