package name.li.pain.users.impl;

import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;

import name.li.pain.users.api.UserService;

public class UserServiceModule extends AbstractModule implements ServiceGuiceSupport {

	@Override
	protected void configure() {
		bindServices(serviceBinding(UserService.class, UserServiceImpl.class));
	}

}
