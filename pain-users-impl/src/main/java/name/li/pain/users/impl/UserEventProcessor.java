package name.li.pain.users.impl;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraReadSideProcessor;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraSession;

import name.li.pain.users.impl.events.TagsUpdatedEvent;
import name.li.pain.users.impl.events.UserCreatedEvent;
import name.li.pain.users.impl.events.UserEvent;

public class UserEventProcessor extends CassandraReadSideProcessor<UserEvent> {

	private PreparedStatement writeOffset;
	private PreparedStatement writeUser;
	private PreparedStatement updateTags;

	@Override
	public AggregateEventTag<UserEvent> aggregateTag() {
		return UserEvent.TAG;
	}

	@Override
	public EventHandlers defineEventHandlers(EventHandlersBuilder builder) {
		builder.setEventHandler(UserCreatedEvent.class, (evt, offset) -> {
			BoundStatement ws = writeUser.bind();
			ws.setString("id", evt.getUserId());
			ws.setString("name", evt.getName());
			
			return completedStatements(Arrays.asList(ws, updateOffset(offset)));
			
		});

		builder.setEventHandler(TagsUpdatedEvent.class, (evt, offset) -> {
			BoundStatement ut = updateTags.bind();
			ut.setString("id", evt.getUserId());
			ut.setSet("tags", evt.getTags().stream().map(e -> e.name()).collect(Collectors.toSet()));

			return completedStatements(Arrays.asList(ut, updateOffset(offset)));
		});
		return builder.build();
	}


	@Override
	public CompletionStage<Optional<UUID>> prepare(CassandraSession session) {
		return session.executeCreateTable("create table if not exists users (partition int, id text, name text, tags set<text>, primary key (partition, id))")
				.thenCompose(done -> session.prepare("insert into users (partition, id, name) values (1, ?, ?)").thenApply(ps -> {
					this.writeUser = ps;
					return done;
				}))
				.thenCompose(done -> session.prepare("update users set tags = ? where partition = 1 and id = ?").thenApply(ps -> {
					this.updateTags = ps;
					return done;
				}))
				.thenCompose(done -> session.executeCreateTable("create table if not exists users_offset(partition int, offset timeuuid, primary key (partition))"))
				.thenCompose(done -> session.prepare("insert into users_offset (partition, offset) values (1, ?)").thenApply(ps -> {
					this.writeOffset = ps;
					return done;
				}))
				.thenCompose(done -> session.selectOne("select offset from users_offset"))
				.thenApply(opt -> opt.map(row -> row.getUUID("offset")));
	}
	

	private BoundStatement updateOffset(UUID offset) {
		BoundStatement wo = writeOffset.bind();
		wo.setUUID("offset", offset);
		return wo;
	}

}
