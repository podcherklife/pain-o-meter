package name.li.pain.users.impl.commands;

public class CreatePainUserCommand implements UserCommand<String> {

	private String name;

	public CreatePainUserCommand(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
