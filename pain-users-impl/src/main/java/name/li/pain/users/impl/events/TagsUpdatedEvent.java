package name.li.pain.users.impl.events;

import java.util.Set;

import name.li.pain.users.api.ImmutableTag;

public class TagsUpdatedEvent extends UserEvent {
	private static final long serialVersionUID = 1L;

	private Set<ImmutableTag> tags;

	public TagsUpdatedEvent(String userId, Set<ImmutableTag> tags) {
		super(userId);
		this.tags = tags;
	}

	public Set<ImmutableTag> getTags() {
		return tags;
	}
}
