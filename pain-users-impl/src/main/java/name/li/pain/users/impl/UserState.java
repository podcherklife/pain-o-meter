package name.li.pain.users.impl;

import java.util.Date;
import java.util.Set;

import name.li.pain.users.api.ImmutableTag;

public class UserState {
	
	private Date creationDate;
	
	private Set<ImmutableTag> tags;
	
	public UserState(Date creationDate, Set<ImmutableTag> tags) {
		super();
		this.tags = tags;
		this.creationDate = creationDate;
	}

	public Set<ImmutableTag> getTags() {
		return tags;
	}

	public Date getCreationDate() {
		return creationDate;
	}

}
