package name.li.pain.users.impl;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRef;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraReadSide;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraSession;

import akka.Done;
import akka.NotUsed;
import name.li.pain.users.api.ImmutableTag;
import name.li.pain.users.api.UserService;
import name.li.pain.users.api.UserState;
import name.li.pain.users.impl.commands.CreatePainUserCommand;
import name.li.pain.users.impl.commands.GetUserState;
import name.li.pain.users.impl.commands.GetUserTags;
import name.li.pain.users.impl.commands.UpdateTagsCommand;
import name.li.pain.users.impl.commands.UserCommand;

public class UserServiceImpl implements UserService {

	private final PersistentEntityRegistry persistentEntityRegistry;
	private final CassandraSession db;

	@Inject
	public UserServiceImpl(PersistentEntityRegistry persistentEntityRegistry, CassandraReadSide readSide,
			CassandraSession db) {
		this.persistentEntityRegistry = persistentEntityRegistry;
		this.db = db;
		persistentEntityRegistry.register(UserEntity.class);
		readSide.register(UserEventProcessor.class);
	}

	@Override
	public ServiceCall<NotUsed, String> newUser(String name) {
		return req -> {
			PersistentEntityRef<UserCommand<?>> ref = persistentEntityRegistry.refFor(UserEntity.class, name);
			return ref.ask(new CreatePainUserCommand(name)).thenApply(String::valueOf);
		};
	}

	@Override
	public ServiceCall<Set<ImmutableTag>, Done> updateTags(String userId) {
		return req -> {
			PersistentEntityRef<UserCommand<?>> ref = persistentEntityRegistry.refFor(UserEntity.class, userId);
			return ref.ask(new UpdateTagsCommand(req));
		};
	}

	@Override
	public ServiceCall<NotUsed, Set<ImmutableTag>> getTags(String userId) {
		return req -> {
			PersistentEntityRef<UserCommand<?>> ref = persistentEntityRegistry.refFor(UserEntity.class, userId);
			return ref.ask(new GetUserTags());
		};
	}

	@Override
	public ServiceCall<NotUsed, UserState> getUser(String userId) {
		return req -> {
			PersistentEntityRef<UserCommand<?>> ref = persistentEntityRegistry.refFor(UserEntity.class, userId);
			return ref.ask(new GetUserState());
		};
	}

	@Override
	public ServiceCall<NotUsed, List<UserState>> getAllUsers() {
		return req -> db.selectAll("select * from users")
				.thenApply(rows -> rows.stream()
						.map(row -> new UserState(
								row.getString("id"),
								row.getString("name"),
								row.getSet("tags", String.class).stream().map(str -> new ImmutableTag.Builder().name(str).build()).collect(Collectors.toSet())))
						.collect(Collectors.toList()));
	}


}
