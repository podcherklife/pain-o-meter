package name.li.pain.users.impl.commands;

import com.lightbend.lagom.javadsl.persistence.PersistentEntity;

public interface UserCommand<T> extends PersistentEntity.ReplyType<T> {

}
