package name.li.pain.users.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.Sets;
import com.lightbend.lagom.javadsl.api.transport.TransportException;

import name.li.pain.users.api.ImmutableTag;
import name.li.pain.users.api.UserService;
import name.li.pain.users.api.UserState;
import name.li.pain.utils.test.LagomServiceTest;

public class UserServiceTest extends LagomServiceTest {
	
	@BeforeClass
	public static void start() {
		setup(defaultSetup());
	}
	
	@AfterClass
	public static void stop() {
		tearDown();
	}

	@Test
	public void shouldCreateUserWithTheSameIdAsName() throws Exception {
		UserService service = server().client(UserService.class);

		String name = buildIdForTest("User");
		String id = get(service.newUser(name).invoke());

		assertEquals(name, id);
	}

	@Test
	public void shouldThowExceptionWhenTryingToCreateUserThatAlreadyExists() throws Exception {
		UserService service = server().client(UserService.class);

		String name = buildIdForTest("User");
		
		get(service.newUser(name).invoke());
		try {
			get(service.newUser(name).invoke());
			fail();
		} catch (ExecutionException e) {
			assertTrue("Unexpected error type", e.getCause() instanceof TransportException);
		}
	}

	@Test
	public void shouldCreateUserWithNoTags() throws Exception {
		UserService service = server().client(UserService.class);

		String name = buildIdForTest("User");

		Set<ImmutableTag> tags = get(service.newUser(name).invoke()
				.thenCompose(id -> service.getTags(id).invoke()));

		assertTrue("Tags for newly created user ure not empty", tags.isEmpty());
	}

	@Test
	public void shouldReturnAllCreatedUsers() throws Exception {
		UserService service = server().client(UserService.class);

		String name = buildIdForTest("User");

		get(service.newUser(name).invoke());
		Thread.sleep(TimeUnit.MILLISECONDS.convert(10, TimeUnit.SECONDS));
		List<UserState> usersAfterCreateEvent = get(service.getAllUsers().invoke());

		assertTrue("Failed to find created user in getAllUsers() result",
				usersAfterCreateEvent.stream().filter(e -> e.getName().equals(name)).findFirst().isPresent());
	}

	@Test
	public void shouldBeAbleToUpdateTags() throws Exception {
		UserService service = server().client(UserService.class);

		HashSet<ImmutableTag> tagsToAdd = Sets.newHashSet(
				new ImmutableTag.Builder().name(buildIdForTest("tag1")).build(),
				new ImmutableTag.Builder().name(buildIdForTest("tag2")).build());

		Set<ImmutableTag> userTags = get(service.newUser(buildIdForTest("user")).invoke()
				.thenCompose(id -> {
					return service.updateTags(id).invoke(tagsToAdd)
							.thenCompose(done -> {
								return service.getTags(id).invoke();
							});
				}));

		assertEquals(tagsToAdd, userTags);
	}

	@Test
	public void sholdReturnUserStateWithTags() throws Exception {
		UserService service = server().client(UserService.class);

		String name = buildIdForTest("User");
		HashSet<ImmutableTag> tags = Sets.newHashSet(
				new ImmutableTag.Builder().name(buildIdForTest("tag1")).build());

		UserState state = get(service.newUser(name).invoke()
				.thenCompose(id -> service.updateTags(id).invoke(tags)
						.thenCompose(done -> service.getUser(id).invoke())));

		assertEquals(name, state.getName());
		assertEquals(tags, state.getTags());

	}

}
